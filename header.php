<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package photogallery
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">


	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<nav>
	<?php
		wp_nav_menu( array(
			'theme_location' => 'menu-1',
			'menu_id'        => 'primary-menu',
		) );
	?>
</nav>
<div class="wrap">
    <input id="slide-1" type="radio" name="slides" checked>
    <section class="slide slide-one">
    	<h1>1066х1421</h1>
    </section>
    <input id="slide-2" type="radio" name="slides">
    <section class="slide slide-two">
    	<h1>1066х1421</h1>
    </section>
    <input id="slide-3" type="radio" name="slides">
    <section class="slide slide-three">
    	<h1>1600х1800</h1>
    </section>
    <input id="slide-4" type="radio" name="slides">
    <section class="slide slide-four">
    	<h1>1800х1600</h1>
    </section>
	<input id="slide-5" type="radio" name="slides">
    <section class="slide slide-five">
    	<h1>2100х2800</h1>
    </section>
	<input id="slide-6" type="radio" name="slides">
    <section class="slide slide-six">
    	<h1>2133х1600</h1>
    </section>
	<input id="slide-7" type="radio" name="slides">
    <section class="slide slide-seven">
    	<h1>2800х2100</h1>
    </section>
	<input id="slide-8" type="radio" name="slides">
    <section class="slide slide-eight">
    	<h1>600x800</h1>
    </section>
	<input id="slide-9" type="radio" name="slides">
    <section class="slide slide-nine">
    	<h1>800x600</h1>
    </section>
	<div class="labels-for-sliders">
   		<label for="slide-1">1</label>
   		<label for="slide-2">2</label>
   		<label for="slide-3">3</label>
   		<label for="slide-4">4</label>
   		<label for="slide-6">5</label>
   		<label for="slide-6">6</label>
   		<label for="slide-7">7</label>
   		<label for="slide-8">8</label>
   		<label for="slide-9">9</label>
   	</div>
</div>